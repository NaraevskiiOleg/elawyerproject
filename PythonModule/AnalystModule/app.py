from __future__ import absolute_import, division, print_function
from tensorflow import keras
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import flask
from flask import Flask

app = Flask(__name__)


def model_build_sale():
    dataframe_new = pd.read_csv("E:/YandexDisk/Магистерская диссертация/Программа/БД/sale.txt", delimiter="\t")
    dataset_new = dataframe_new.values
    train_data_new = dataset_new

    x_train = dataset_new[:, :15]
    y_train = dataset_new[:, 15:]

    model = keras.Sequential([
        keras.layers.Dense(15, activation='tanh',
                           input_shape=(x_train.shape[1],)),
        keras.layers.Dense(64, activation='tanh'),
        keras.layers.Dense(1, activation='tanh')
    ])
    optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae'])
    return model


def model_build_rent():
    dataframe_new = pd.read_csv("E:/YandexDisk/Магистерская диссертация/Программа/БД/rent.txt", delimiter="\t")
    dataset_new = dataframe_new.values
    train_data_new = dataset_new

    x_train = dataset_new[:, :11]
    y_train = dataset_new[:, 11:]

    model = keras.Sequential([
        keras.layers.Dense(10, activation='tanh',
                           input_shape=(x_train.shape[1],)),
        keras.layers.Dense(64, activation='tanh'),
        keras.layers.Dense(1, activation='tanh')
    ])
    optimizer = keras.optimizers.Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae'])
    return model


@app.route('/')
def itworks():
    return 'Hello! App is just now works!'


@app.route("/predictSales", methods=["POST"])
def predict_sale():
    data = {"success": False, "msg": ""}
    model = model_build_sale()
    model.summary()
    if flask.request.method == "POST":
        json = flask.request.get_json()
        jsondata = [[json["data"]]]
        result = model.predict(jsondata)
        data["success"] = True
        data["msg"] = "Theory = " + str(result)
    # return the data dictionary as a JSON response
    return flask.jsonify(data)


@app.route("/predictRent", methods=["POST"])
def predict_rent():
    data = {"success": False, "msg": ""}
    model = model_build_sale()
    model.summary()
    if flask.request.method == "POST":
        if flask.request.is_json():
            json = flask.request.get_json()
            ldata = [[json["data"]]]
            result = model.predict(ldata)
            data["success"] = True
            data["msg"] = "Theory = " + str(result)
    # return the data dictionary as a JSON response
    return flask.jsonify(data)


if __name__ == '__main__':
    app.run()
