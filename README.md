# ELawyerProject

Проект "Электронный адвокат". Пилотный выпуск.

Данный проект занимается проблемами обеспечения законности сделок в сфере недвижимости.
Основными вопросами, обсуждаемыми в данном проекте, являются анализ автоматизации, изучение нейросетевых технологий и их применения в данной области, анализ и применение технологии распределенного реестра, разработка реализации основных функций системы и разработка программного обеспечения.


E-Lawyer Project. Pilot issue. 

This project deals with the problems of ensuring the legality of transactions in the field of real estate.
The main issues discussed in this project are the analysis of automation, the study of neural network technologies and their applications in this area, the analysis and application of distributed registry technology, the development of the implementation of the main functions of the system and software development.


