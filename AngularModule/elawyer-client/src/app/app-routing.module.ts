import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TypographyComponent } from './typography/typography.component';
import { RefbooksComponent } from './refbooks/refbooks.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ContractsComponent } from './contracts/contracts.component';
import { AuxsystemComponent } from './auxsystem/auxsystem.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'typography', component: TypographyComponent },
  { path: 'refbooks', component: RefbooksComponent },
  { path: 'analytics', component: AnalyticsComponent },
  { path: 'contracts', component: ContractsComponent },
  { path: 'auxsystem', component: AuxsystemComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
