import { TestBed } from '@angular/core/testing';

import { AuxSystemsInteractionService } from './aux-systems-interaction.service';

describe('AuxSystemsInteractionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuxSystemsInteractionService = TestBed.get(AuxSystemsInteractionService);
    expect(service).toBeTruthy();
  });
});
