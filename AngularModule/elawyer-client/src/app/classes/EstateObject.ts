export class SearchEstateObjectResult {
  public objects: EstateObject[];
  public grouped: boolean;
  public mode: string;
  public found: number;
  public found_all: boolean;
  public region: string;
  public error: Errors[];
}

export class EstateObject {
  public ADDRESS: string;
  public CADNOMER: string;
  public TYPE: string;
  public AREA: string;
  public CATEGORY: string;
}

export class Errors {
  public code: number;
  public mess: string;
}
