import { EstateObject} from './EstateObject';

export const ESTATES: EstateObject[] = [
  { ADDRESS: 'г Новочеркасск, ул Крылова, д. 6, 10', CADNOMER: '61:55:0010437:1041', TYPE: '', AREA: '', CATEGORY: '' },
  { ADDRESS: 'г Новочеркасск, ул Крылова, д. 6, 13', CADNOMER: '61:55:0010437:1053', TYPE: '', AREA: '', CATEGORY: '' },
  { ADDRESS: 'г Новочеркасск, ул Крылова, д. 6, 15', CADNOMER: '61:55:0010437:1055', TYPE: '', AREA: '', CATEGORY: '' },
  { ADDRESS: 'г Новочеркасск, ул Крылова, д. 6, 18', CADNOMER: '61:55:0010437:991', TYPE: '', AREA: '', CATEGORY: '' },
  { ADDRESS: 'г Новочеркасск, ул Крылова, д. 6, 2', CADNOMER: '61:55:0010437:984', TYPE: '', AREA: '', CATEGORY: '' },
  { ADDRESS: 'Ростовская обл., г. Новочеркасск, ул. Крылова,  6', CADNOMER: '61:55:0010437:21', TYPE: '', AREA: '', CATEGORY: '' }
];
