export class SaleModel {
  public ownership: number;
  public contract_of_sale: number;
  public concluded_laws: number;
  public contract_observed: number;
  public payment_contract: number;
  public act: number;
  public ownership_it: number;
  public facts_imaginary_feigned_transactions: number;
  public discharged_from_house_book: number;
  public rights_family: number;
  public objections_co_owners: number;
  public priority_co_owner: number;
  public condition_subject: number;
  public fact_bearing: number;

  constructor() {
    this.ownership = 0;
    this.contract_of_sale = 0;
    this.concluded_laws = 0;
    this.contract_observed = 0;
    this.payment_contract = 0;
    this.act = 0;
    this.ownership_it = 0;
    this.facts_imaginary_feigned_transactions = 0;
    this.discharged_from_house_book = 0;
    this.rights_family = 0;
    this.objections_co_owners = 0;
    this.priority_co_owner = 0;
    this.condition_subject = 0;
    this.fact_bearing = 0;
  }
}
