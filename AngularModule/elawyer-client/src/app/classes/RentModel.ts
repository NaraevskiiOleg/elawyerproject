export class RentModel {
  public ownership: number;
  public contract_of_rent: number;
  public purpose_lease: number;
  public prolongation_conditions: number;
  public payment_contract: number;
  public all_people_included: number;
  public act_use: number;
  public objections_co_owners: number;
  public condition_subject: number;
  public payment_utility_services: number;

  constructor() {
    this.ownership = 0;
    this.contract_of_rent = 0;
    this.purpose_lease = 0;
    this.prolongation_conditions = 0;
    this.payment_contract = 0;
    this.all_people_included = 0;
    this.act_use = 0;
    this.objections_co_owners = 0;
    this.condition_subject = 0;
    this.payment_utility_services = 0;
  }
}
