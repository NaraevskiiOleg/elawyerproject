import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { TypographyComponent } from './typography/typography.component';
import { RefbooksComponent } from './refbooks/refbooks.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { ContractsComponent } from './contracts/contracts.component';
import { AuxsystemComponent } from './auxsystem/auxsystem.component';
import {HttpClientModule} from '@angular/common/http';
import {AuxSystemsInteractionService} from './aux-systems-interaction.service';
import {AnaliticsInteractionService} from './analitics-interaction.service';
import {GlobalService} from './global.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    FooterComponent,
    TypographyComponent,
    RefbooksComponent,
    AnalyticsComponent,
    ContractsComponent,
    AuxsystemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule.forRoot()
  ],
  providers: [GlobalService, AuxSystemsInteractionService, AnaliticsInteractionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
