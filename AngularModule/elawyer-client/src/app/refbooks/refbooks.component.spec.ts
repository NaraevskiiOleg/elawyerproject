import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RefbooksComponent } from './refbooks.component';

describe('RefbooksComponent', () => {
  let component: RefbooksComponent;
  let fixture: ComponentFixture<RefbooksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefbooksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefbooksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
