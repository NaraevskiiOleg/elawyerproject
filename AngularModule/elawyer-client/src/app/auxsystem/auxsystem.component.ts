import { Component } from '@angular/core';
import {SearchEstateObject} from '../classes/SearchEstateObject';
import {AuxSystemsInteractionService} from '../aux-systems-interaction.service';
import { ESTATES } from '../classes/mock-estateObject';
import {SearchEstateObjectResult} from '../classes/EstateObject';

@Component({
  selector: 'app-auxsystem',
  templateUrl: './auxsystem.component.html',
  styleUrls: ['./auxsystem.component.scss']
})
export class AuxsystemComponent {
  public estates: SearchEstateObjectResult = new SearchEstateObjectResult();

  constructor(private auxHttpService: AuxSystemsInteractionService) { }

  public searchData: SearchEstateObject = new SearchEstateObject();

  public searchObjectCall(sData: SearchEstateObject) {
    this.auxHttpService.searchObject(sData).subscribe((result) => {
      console.log(result.toString()); this.estates = result as SearchEstateObjectResult;
    }, (err) => {
      console.log(err);
    });
  }

  public getMoreDetails(cadNum: string) {
    this.auxHttpService.objectDetails(cadNum);
  }

  public testApiCall() {
    this.auxHttpService.testApi().subscribe((result) => {
      console.log(result.toString());
    }, (err) => {
      console.log(err);
    });
  }
}
