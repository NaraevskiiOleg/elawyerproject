import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuxsystemComponent } from './auxsystem.component';

describe('AuxsystemComponent', () => {
  let component: AuxsystemComponent;
  let fixture: ComponentFixture<AuxsystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuxsystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuxsystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
