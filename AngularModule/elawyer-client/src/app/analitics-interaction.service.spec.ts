import { TestBed } from '@angular/core/testing';

import { AnaliticsInteractionService } from './analitics-interaction.service';

describe('AnaliticsInteractionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AnaliticsInteractionService = TestBed.get(AnaliticsInteractionService);
    expect(service).toBeTruthy();
  });
});
