import { Component, OnInit } from '@angular/core';
import {SaleModel} from '../classes/SaleModel';
import {RentModel} from '../classes/RentModel';
import {SearchEstateObject} from '../classes/SearchEstateObject';
import {SearchEstateObjectResult} from '../classes/EstateObject';
import {AnaliticsInteractionService} from '../analitics-interaction.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  public SaleModel: SaleModel = new SaleModel();
  public RentModel: RentModel = new RentModel();

  public saleClaimResult: any;

  constructor(private analiticService: AnaliticsInteractionService) { }

  ngOnInit() {
  }

  public GetSaleClaimResult(model: SaleModel) {
    this.analiticService.postSaleClaim(model).subscribe((result) => {
      console.log(result.toString()); this.saleClaimResult = result as any;
    }, (err) => {
      console.log(err);
    });
  }

  public GetRentClaimResult(model: RentModel) {
    this.analiticService.postRentClaim(model).subscribe((result) => {
      console.log(result.toString()); this.saleClaimResult = result as any;
    }, (err) => {
      console.log(err);
    });
  }
}
