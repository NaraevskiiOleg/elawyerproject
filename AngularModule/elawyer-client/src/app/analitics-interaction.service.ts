import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GlobalService} from './global.service';
import {SearchEstateObject} from './classes/SearchEstateObject';
import {catchError, tap} from 'rxjs/operators';
import {SaleModel} from './classes/SaleModel';
import {RentModel} from './classes/RentModel';

@Injectable({
  providedIn: 'root'
})
export class AnaliticsInteractionService {
  private endpoint = 'http://localhost/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient, private global: GlobalService) { }

  public postSaleClaim(sale: SaleModel) {
    return this.http.post(this.endpoint , sale, this.httpOptions).pipe(
      tap((object) => console.log(object.toString())),
      catchError(this.global.handleError<any>('postSaleClaim'))
    );
  }

  public postRentClaim(rent: RentModel) {
    return this.http.post(this.endpoint , rent, this.httpOptions).pipe(
      tap((object) => console.log(object.toString())),
      catchError(this.global.handleError<any>('postSaleClaim'))
    );
  }
}
