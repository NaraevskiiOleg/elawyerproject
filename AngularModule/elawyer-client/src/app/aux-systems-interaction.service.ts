import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SearchEstateObject} from './classes/SearchEstateObject';
import {SearchEstateObjectResult} from './classes/EstateObject';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {GlobalService} from './global.service';

@Injectable({
  providedIn: 'root'
})
export class AuxSystemsInteractionService {
  private authToken = 'L3CH-JNIT-D98Q-URZN';

  private endpoint = 'https://apirosreestr.ru/api/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Token': this.authToken
    })
  };

  constructor(private http: HttpClient, private global: GlobalService) {

  }

  public searchObject(searchData: SearchEstateObject) {
   const body = {
      query: searchData.CadNumber === null ? searchData.CadNumber : searchData.Address,
      mode: 'lite', grouped: 5
    };

    return this.http.post(this.endpoint + 'cadaster/search', body, this.httpOptions).pipe(
      tap((object) => console.log(object.toString())),
      catchError(this.global.handleError<any>('searchObject'))
    );
  }

  public testApi(): Observable<any> {
    return this.http.get<any>(this.endpoint + 'account/info', this.httpOptions).pipe(
      tap((testInfo) => console.log(testInfo.toString())),
      catchError(this.global.handleError<any>('testApi'))
    );
  }


  public objectDetails(queryData: string) {
    const body = {
      query: queryData
    };

    return this.http.post(this.endpoint + 'cadaster/search', body, this.httpOptions).pipe(
      tap((details) => console.log(details.toString())),
      catchError(this.global.handleError<any>('objectDetails'))
    );
  }
}
